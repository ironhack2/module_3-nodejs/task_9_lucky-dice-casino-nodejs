//  Khai báo thư viện express
const express = require('express');

// Import Middleware
const { userMiddleware } = require('../middleware/userMiddleware');


// Import
const { createUser, getAllUser, getUserById, updateUserById, deleteUserById } = require('../controller/userController');


//Tạo router
const userRouter = express.Router();


//  Sử dụng Middleware
userRouter.use(userMiddleware);


//CREATE A USER
userRouter.post('/users', createUser);


//GET ALL USER
userRouter.get('/users', getAllUser);


//GET A USER
userRouter.get('/users/:userId', getUserById)


//UPDATE A USER
userRouter.put('/users/:userId', updateUserById)


//DELETE A USER
userRouter.delete('/users/:userId', deleteUserById)

module.exports = userRouter

// {
//     "username": "Hoàng Béo",
//     "firstname": "Lê",
//     "lastname":"Hoàng"
// }