//Khai báo thư viện express
const express = require('express');

//Import Middleware
const { voucherHistoryMiddleware } = require('../middleware/voucherHistoryMiddleware');

//Import Controller
const { createVoucherHistory, getAllVoucherHistory, getVoucherHistoryById, updateVoucherHistory, deleteVoucherHistory } = require('../controller/voucherHistoryController')

//Tạo Router
const voucherHistoryRouter = express.Router();

//Sử dụng middleware
voucherHistoryRouter.use(voucherHistoryMiddleware);

//Khai báo API

//PORT
voucherHistoryRouter.post('/voucher-histories', createVoucherHistory);

//GET ALL
voucherHistoryRouter.get('/voucher-histories', getAllVoucherHistory);

//GET BY ID
voucherHistoryRouter.get('/voucher-histories/:voucherHistoryId', getVoucherHistoryById);

//UPDATE BY ID
voucherHistoryRouter.put('/voucher-histories/:voucherHistoryId', updateVoucherHistory);


//DELETE BY ID
voucherHistoryRouter.delete('/voucher-histories/:voucherHistoryId', deleteVoucherHistory);



// EXPORT
module.exports = voucherHistoryRouter;



// {
//     "code":"abc",
//     "discount":20,
//     "node":"123abc"
// }