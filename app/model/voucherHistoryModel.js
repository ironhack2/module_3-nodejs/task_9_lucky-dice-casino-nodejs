//b1: import mongoose
const mongoose = require("mongoose");

//b2: khai báo Schema
const Schema = mongoose.Schema;

//b3: khởi tạo Schema
const voucherHistorySchema = new Schema({
    _id: mongoose.Types.ObjectId,
    user: {
        type: mongoose.Types.ObjectId,
        ref: "user",
        required: true
    },
    voucher: {
        type: mongoose.Types.ObjectId,
        ref: "voucher",
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now(),
    },
    updatedAt: {
        type: Date,
        default: Date.now(),
    }
})

//b4: export ra model
module.exports = mongoose.model("voucherHistory", voucherHistorySchema);