//b1: import mongoose
const mongoose = require("mongoose");

//b2: khai báo Schema
const Schema = mongoose.Schema;

//b3: khởi tạo Schema
const diceHistorySchema = new Schema({
    _id: {
        type: mongoose.Types.ObjectId,
    },
    user: {
        type: mongoose.Types.ObjectId,
        ref: "user",
        required: true
    },
    dice: {
        type: Number,
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now(),
    },
    updatedAt: {
        type: Date,
        default: Date.now(),
    }
})

//b4: export ra model
module.exports = mongoose.model("diceHistory", diceHistorySchema);