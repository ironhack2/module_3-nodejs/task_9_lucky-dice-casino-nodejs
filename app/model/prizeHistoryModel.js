//b1: import mongoose
const mongoose = require("mongoose");

//b2: khai báo Schema
const Schema = mongoose.Schema;

//b3: khởi tạo Schema
const prizeHistorySchema = new Schema({
    _id: mongoose.Types.ObjectId,
    user: {
        type: mongoose.Types.ObjectId,
        ref: "user",
        required: true
    },
    prize: {
        type: mongoose.Types.ObjectId,
        ref: "prize",
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now(),
    },
    updatedAt: {
        type: Date,
        default: Date.now(),
    }
})

//b4: export ra model
module.exports = mongoose.model("prizeHistory", prizeHistorySchema);