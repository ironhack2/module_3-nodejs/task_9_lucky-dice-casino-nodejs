 /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */

 const gREQUEST_STATUS_OK = 200;
 const gREQUEST_READY_STATUS_FINISH_AND_OK = 4;

 //base url
 const vBASE_URL = "/devcamp-lucky-dice";
 const vUTF8_TEXT_APPLICATION_HEADER = "application/json;charset=UTF-8";


 // Tạo biến toàn cực đối tượng để truyền dữ liệu giữa các bước
 const gUserObj = {
         username: "",
         firstname: "",
         lastname: ""
     }
     /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */

 $(document).ready(function() {
     $('#btn-dice').on('click', function() {
         onBtnNemClick()
     })

 });


 /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
 function onBtnNemClick() {

     //  Bước 1 :  Thu thập dữ liệu
     getData(gUserObj)
         //    Bước 2 : Kiểm tra dữ liệu
     var vIsCheck = validateData(gUserObj);
     if (vIsCheck) {

         //   Bước 3 : gọi API sever
         callApiSeverDice(gUserObj);
     }
 }


 // HÀM SỰ KIỆN NÚT DICE HISTORY
 $('#btn-dice-history').on('click', function() {

     console.log("%c Nút Dice History được ấn", "color:red");

     //  Bước 1 :  Thu thập dữ liệu
     getData(gUserObj);
     //    Bước 2 : Kiểm tra dữ liệu
     var vIsCheck = validateData(gUserObj);
     if (vIsCheck) {

         callApiHistoryDice(gUserObj);

     }
 })



 // HÀM SỰ KIỆN NÚT VOUCHER HISTORY
 $('#btn-voucher-history').on('click', function() {

     console.log("%c Nút Dice History được ấn", "color:red");

     //  Bước 1 :  Thu thập dữ liệu
     getData(gUserObj);
     //    Bước 2 : Kiểm tra dữ liệu
     var vIsCheck = validateData(gUserObj);
     if (vIsCheck) {

         //   Bước 3 : gọi API sever
         callApiHistoryVoucher(gUserObj);
     }
 })


 // HÀM SỰ KIỆN NÚT  PRIZE  HISTORY
 $('#btn-prize-history').on('click', function() {

     console.log("%c Nút Dice History được ấn", "color:red");
     //  Bước 1 :  Thu thập dữ liệu
     getData(gUserObj);
     //    Bước 2 : Kiểm tra dữ liệu
     var vIsCheck = validateData(gUserObj);
     if (vIsCheck) {

         //   Bước 3 : gọi API sever
         callApiHistoryPrize(gUserObj);
     }
 })



 /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/

 //CÁC HÀM XÚC XẮC

 function getData(paramDataUser) {
     var vInputUserName = $("#inp_user_name");
     var vInputFirstName = $("#inp_first_name");
     var vInputLastName = $("#inp_last_name");

     paramDataUser.username = vInputUserName.val().trim();
     paramDataUser.firstname = vInputFirstName.val().trim();
     paramDataUser.lastname = vInputLastName.val().trim();

 }

 //         Hàm kiểm tra dữ liệu
 function validateData(paramDataUser) {

     if (paramDataUser.username == "") {
         $('#inp_user_name').addClass("is-invalid").removeClass("is-valid");
         $('#user_name_false').html("Hãy nhập user name");
         console.log("Cần nhập User Name!!!");
         // alert("Cần nhập User Name!!!");
         return false;

     } else {
         $('#inp_user_name').addClass("is-valid").removeClass("is-invalid");
         $('#user_name_false').hide();
     }




     if (paramDataUser.firstname == "") {
         $('#inp_first_name').addClass("is-invalid").removeClass("is-valid");
         $('#first_name_false').html("Hãy nhập first name");
         console.log("Cần nhập First Name!!!");
         // alert("Cần nhập First Name!!!");
         return false;

     } else {
         $('#inp_first_name').addClass("is-valid").removeClass("is-invalid");
         $('#first_name_false').hide();
     }



     if (paramDataUser.lastname == "") {
         $('#inp_last_name').addClass("is-invalid").removeClass("is-valid");
         $('#last_name_false').html("Hãy nhập last name");
         console.log("Cần nhập Last Name!!!");
         // alert("Cần nhập Last Name!!!");
         return false;

     } else {
         $('#inp_last_name').addClass("is-valid").removeClass("is-invalid");
         $('#last_name_false').hide();
     }

     return true;
 }


 // Hàm call API
 function callApiSeverDice(paramDataUser) {

     $.ajax({
         url: vBASE_URL + "/dice",
         type: "POST",
         contentType: "application/json;charset=UTF-8",
         data: JSON.stringify(paramDataUser),
         success: function(response) {
             console.log(response);

             //Hiển thị
             processResponse(response);
         },
         error: function(ajaxContent) {
             console.log(ajaxContent);
         }
     })
 }



 //      CÁC HÀM XỬ LÝ RESPONSE TRẢ VỀ THÀNH CÔNG
 function processResponse(paramXmlHttpDice) {
     "user strict";

     console.log(paramXmlHttpDice);

     //  get result dice
     var vDiceReady = paramXmlHttpDice.dice;
     console.log("dice result: " + vDiceReady);

     // gọi hàm hiển thị dice
     changeDice(vDiceReady);

     //gọi hàm hiển thị lời nhắn
     changeThongBao(vDiceReady);

     //gọi hàm hiển thị voucher
     changeVoucherId(paramXmlHttpDice);

     //gọi hàm hiển thị ảnh phần thưởng
     changeGift(paramXmlHttpDice);
 }

 function changeDice(paramDice) {
     "user strict";
     var vImgDice = $("#img-dice");

     switch (paramDice) {
         case 1:
             vImgDice.attr("src", "images/1.png");
             break;
         case 2:
             vImgDice.attr("src", "images/2.png");
             break;
         case 3:
             vImgDice.attr("src", "images/3.png");
             break;
         case 4:
             vImgDice.attr("src", "images/4.png");
             break;
         case 5:
             vImgDice.attr("src", "images/5.png");
             break;
         case 6:
             vImgDice.attr("src", "images/6.png");
             break;
     }
 }


 function changeThongBao(paramDice) {
     "user strict";

     // hiển thị thẻ P chúc mừng
     var vElementPChucMung = $("#p_none");
     vElementPChucMung.css("display", "block");

     // Hiển thị lời chúc theo số điểm xúc xắc
     var vThongBao = $("#p-notification-dice");
     if (paramDice < 4) {
         vThongBao.html("Chúc bạn may mắn lần sau !!!");
         vThongBao.css("color", "blue");
     } else {
         vThongBao.html("Chúc mừng bạn hãy chơi tiếp lần nữa nha !!!")
         vThongBao.css({
             "color": "red",
             "font-weight": "bold"
         });
     }
 }


 function changeVoucherId(paramResponseObj) {
     "user strict";

     var vPVoucherId = $("#p-voucher-id");
     var vPVoucherPercent = $("#p-voucher-percent");

     if (paramResponseObj.voucher != null) {
         vPVoucherId.html(`<h3><b class="w3-text-red">ID: ${ paramResponseObj.voucher.code}</b></h3>`);
         vPVoucherPercent.html(`<h3><b class="w3-text-red">Giảm giá: ${ paramResponseObj.voucher.discount}%</b></h3>`);
     } else {
         vPVoucherId.html(`<h3><b class="w3-text-red">ID: ....</b></h3>`);
         vPVoucherPercent.html(`<h3><b class="w3-text-red">Giảm giá: 0%</b></h3>`);
     }

     if (paramResponseObj.prize != null) {
         $("#p-present").html(`<h3><b class="w3-text-red">Phần thưởng: ${ paramResponseObj.prize}</b></h3>`);
     } else {
         $("#p-present").html("Không có phần thưởng")
     }
 }

 function changeGift(paramResponePrize) {
     var vImgPresent = $("#img-present");
     var vTenPhanThuong = paramResponePrize.prize;

     switch (vTenPhanThuong) {
         case "Mũ":
             vImgPresent.attr("src", "images/hat.jpg");
             break;
         case "Áo":
             vImgPresent.attr("src", "images/t-shirt.jpg");
             break;
         case "Xe Máy":
             vImgPresent.attr("src", "images/motobike.jpg");
             break;
         case "Ô tô":
             vImgPresent.attr("src", "images/car.jpg");
             break;
         default:
             vImgPresent.attr("src", "images/no-present.jpg");
     }
 }


 //  HISTORY DICE -- LỊCH SỬ TUNG XÚC XẮC:

 // Hàm gọi API lấy lịch xử tung xức xắc từ sever
 function callApiHistoryDice(paramDataUser) {

     $.ajax({
         url: vBASE_URL + "/dice-history?username=" + paramDataUser.username,
         type: "GET",
         //  async: false,
         //  contentType: "application/json;charset=UTF-8",
         data: JSON.stringify(paramDataUser),
         success: function(response) {
             console.log(response);

             //Hiển thị
             showDiceHistory(response);

         },
         error: function(ajaxContent) {
             console.log(ajaxContent);
         }
     })
 }

 // Hàm hiển thị lịch sử tung xúc xắc
 function showDiceHistory(paramResposeDiceHistory) {

     var vHistoryTable = $('#history-placeholder-table');
     $('#history-placeholder-table').empty();

     var bThead = $('<thead>').appendTo(vHistoryTable);
     var bNewRowTh = $("<tr>").appendTo(bThead);
     $('<th>', { html: "Lượt" }).appendTo(bNewRowTh);
     $('<th>', { html: "Dice" }).appendTo(bNewRowTh);

     for (let bI = 0; bI < paramResposeDiceHistory.data.length; bI++) {
         var bTbody = $('<tbody>').appendTo(vHistoryTable);
         var bNewRowTd = $("<tr>").appendTo(bTbody);
         $('<td>', { html: bI + 1 }).appendTo(bNewRowTd);
         $('<td>', { html: paramResposeDiceHistory.data[bI].dice }).appendTo(bNewRowTd);
     }
 }



 //  VOUCHER DICE -- LỊCH SỬ VOUCHER:

 // Hàm gọi API lấy lịch xử tung xức xắc từ sever
 function callApiHistoryVoucher(paramDataUser) {

     $.ajax({
         url: vBASE_URL + "/voucher-history?username=" + paramDataUser.username,
         type: "GET",
         //  async: false,
         //  contentType: "application/json;charset=UTF-8",
         data: JSON.stringify(paramDataUser),
         success: function(response) {
             console.log(response);

             //Hiển thị
             showVoucherHistory(response);

         },
         error: function(ajaxContent) {
             console.log(ajaxContent);
         }
     })
 }

 // Hàm hiển thị lịch sử VOUCHER
 function showVoucherHistory(paramResposeVoucherHistory) {

     var vHistoryTable = $('#history-placeholder-table');
     $('#history-placeholder-table').empty();

     // Tạo Thead
     var bThead = $('<thead>').appendTo(vHistoryTable);
     var bNewRowTh = $("<tr>").appendTo(bThead);
     $('<th>', { html: "Mã voucher" }).appendTo(bNewRowTh);
     $('<th>', { html: "Phần trăm giảm giá" }).appendTo(bNewRowTh);
     $('<th>', { html: "Ghi chú" }).appendTo(bNewRowTh);
     //  $('<th>', { html: "Ngày tạo" }).appendTo(bNewRowTh);
     $('<th>', { html: "Ngày cập nhật" }).appendTo(bNewRowTh);

     //Tạo Tbody
     for (let bI = 0; bI < paramResposeVoucherHistory.voucher.length; bI++) {
         var bTbody = $('<tbody>').appendTo(vHistoryTable);
         var bNewRowTd = $("<tr>").appendTo(bTbody);
         $('<td>', { html: paramResposeVoucherHistory.voucher[bI].code }).appendTo(bNewRowTd);
         $('<td>', { html: paramResposeVoucherHistory.voucher[bI].discount + "%" }).appendTo(bNewRowTd);
         $('<td>', { html: paramResposeVoucherHistory.voucher[bI].note }).appendTo(bNewRowTd);
         //  $('<td>', { html: paramResposeVoucherHistory.voucher[bI].createAt }).appendTo(bNewRowTd);
         $('<td>', { html: paramResposeVoucherHistory.voucher[bI].updatedAt }).appendTo(bNewRowTd);

     }
 }


 //  VOUCHER DICE -- LỊCH SỬ VOUCHER:

 // Hàm gọi API lấy lịch xử tung xức xắc từ sever
 function callApiHistoryPrize(paramDataUser) {

     $.ajax({
         url: vBASE_URL + "/prize-history?username=" + paramDataUser.username,
         type: "GET",
         //  async: false,
         //  contentType: "application/json;charset=UTF-8",
         data: JSON.stringify(paramDataUser),
         success: function(response) {
             console.log(response);

             //Hiển thị
             showPrizeHistory(response);

         },
         error: function(ajaxContent) {
             console.log(ajaxContent);
         }
     })
 }

 // Hàm hiển thị lịch sử VOUCHER
 function showPrizeHistory(paramResposePrizeHistory) {

     var vHistoryTable = $('#history-placeholder-table');
     $('#history-placeholder-table').empty();

     // Tạo Thead
     var bThead = $('<thead>').appendTo(vHistoryTable);
     var bNewRowTh = $("<tr>").appendTo(bThead);
     $('<th>', { html: "Lượt" }).appendTo(bNewRowTh);
     $('<th>', { html: "Quà tặng" }).appendTo(bNewRowTh);
     $('<th>', { html: "Ảnh quà tặng" }).appendTo(bNewRowTh);

     //Tạo Tbody
     for (let bI = 0; bI < paramResposePrizeHistory.prize.length; bI++) {
         var bTbody = $('<tbody>').appendTo(vHistoryTable);
         var bNewRowTd = $("<tr>").appendTo(bTbody);
         $('<td>', { html: bI + 1 }).appendTo(bNewRowTd);
         $('<td>', { html: paramResposePrizeHistory.prize[bI] }).appendTo(bNewRowTd).css({ "font-size": "24px", "color": "red" });
         $("<img>").attr("src", prizePhoto(paramResposePrizeHistory.prize[bI]))
             .appendTo($("<td>").appendTo(bNewRowTd)).css("width", "80px")
     }

     function prizePhoto(paramPrize) {
         switch (paramPrize) {
             case "Mũ":
                 return "./images/hat.jpg";
                 break;
             case "Áo":
                 return "./images/t-shirt.jpg";
                 break;
             case "Xe Máy":
                 return "./images/motobike.jpg";
                 break;
             case "Ô tô":
                 return "./images/car.jpg";
                 break;
             default:
                 return "./images/no-present.jpg";
         }
     }
 }